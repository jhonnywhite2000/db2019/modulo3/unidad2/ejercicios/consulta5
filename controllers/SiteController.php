<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Equipo;
use app\models\Maillot;
use app\models\Lleva;
use app\models\Puerto;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function  actionCrud(){
        return $this->render("gestion");
    }
    public function actionConsulta1() {
      // mediante DAO
         $numero=Yii::$app->
              db->
         createCommand("SELECT distinct count(*) FROM  ciclista
            LEFT JOIN etapa  ON etapa.dorsal = ciclista.dorsal WHERE etapa.dorsal IS null")
              ->queryScalar();
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT distinct nombre, edad FROM  ciclista LEFT JOIN etapa  ON etapa.dorsal = ciclista.dorsal
                WHERE etapa.dorsal IS null',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
            "enunciado"=>"Nombre y edad de los ciclistas que no han ganado etapas",
             "titulo"=>"Consulta 1",
            "sql"=>"SELECT distinct nombre, edad FROM  ciclista LEFT JOIN etapa  ON etapa.dorsal = ciclista.dorsal
                WHERE etapa.dorsal IS null;",
        ]);
    }
    public function  actionConsulta1a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
        'query' => Ciclista::find()
                ->distinct()
                ->select("nombre,edad")
                ->join('LEFT  JOIN', 'etapa', 'etapa.dorsal = ciclista.dorsal')
                ->where("etapa.dorsal is null"),
                ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
             "enunciado"=>"Nombre y edad de los ciclistas que no han ganado etapas",
             "titulo"=>"Consulta 1",
            "sql"=>"SELECT distinct nombre, edad FROM  ciclista LEFT JOIN etapa  ON etapa.dorsal = ciclista.dorsal
                WHERE etapa.dorsal IS null",
        ]);
    }
    public function  actionConsulta2(){
      // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT distinct count(*) FROM  ciclista
                LEFT JOIN puerto  ON puerto.dorsal = ciclista.dorsal
                WHERE puerto.dorsal IS null")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT distinct nombre, edad FROM  ciclista
            LEFT JOIN puerto  ON puerto.dorsal = ciclista.dorsal
            WHERE puerto.dorsal IS null',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
            "enunciado"=>"Nombre y edad de los ciclistas que no han ganado puertos",
             "titulo"=>"Consulta 2",
            "sql"=>"SELECT distinct nombre, edad FROM  ciclista LEFT JOIN puerto  ON puerto.dorsal = ciclista.dorsal
            WHERE puerto.dorsal IS null;",
        ]);
    }
    public function  actionConsulta2a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
                ->distinct()
                ->select("nombre,edad")
                ->leftJoin('puerto', 'puerto.dorsal = ciclista.dorsal')
                ->where("puerto.dorsal is null"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['nombre', 'edad'], 
            "enunciado"=>"Nombre y edad de los ciclistas que no han ganado puertos",
             "titulo"=>"Consulta 2",
            "sql"=>"SELECT distinct nombre, edad FROM  ciclista LEFT JOIN puerto  ON puerto.dorsal = ciclista.dorsal
            WHERE puerto.dorsal IS null",
        ]);
    }
    
           
    public function  actionConsulta3(){
        // mediante DAO
        $numero=Yii::$app->
              db->
              createCommand("SELECT DISTINCT count(*) FROM equipo INNER JOIN (ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal) ON
                equipo.nomequipo = ciclista.nomequipo WHERE etapa.dorsal IS null;")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal) ON
            equipo.nomequipo = ciclista.nomequipo WHERE etapa.dorsal IS null',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['director'], 
            "enunciado"=>"Listar el director de los equipos que tengan ciclistas que no hayan ganado alguna etapa",
             "titulo"=>"Consulta 4",
            "sql"=>"SELECT DISTINCT equipo.director FROM equipo INNER JOIN (ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal) ON
            equipo.nomequipo = ciclista.nomequipo WHERE etapa.dorsal IS null;",
        ]);
     
    }
    public function  actionConsulta3a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Equipo::find()
                ->select("director")
                ->distinct()
                ->innerJoinWith("ciclistas")
                ->leftJoin("etapa","ciclista.dorsal=etapa.dorsal"),
        ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['director'], 
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos",
             "titulo"=>"Consulta 3",
            "sql"=>" SELECT DISTINCT nombre, edad FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal)
                INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal",
        ]); 
        
        $dataProvider = new ActiveDataProvider([
            'query' => Equipo::find()
                ->select("director")
                ->distinct()
                ->joinWith("etapas",true,"left join"),
        ]);
        
        
        
        

        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['director'], 
            "enunciado"=>"Nombre y edad de los ciclistas que han ganado etapas y puertos",
             "titulo"=>"Consulta 3",
            "sql"=>" SELECT DISTINCT nombre, edad FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal)
                INNER JOIN puerto ON ciclista.dorsal = puerto.dorsal",
        ]); 
    }    
    public function  actionConsulta4(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT DISTINCT count(*) FROM ciclista 
                left JOIN lleva ON ciclista.dorsal = lleva.dorsal
                WHERE lleva.dorsal IS null;")
              ->queryScalar();
        
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista left JOIN lleva ON ciclista.dorsal = lleva.dorsal
            WHERE lleva.dorsal IS null',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre'], 
            "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado algún maillot",
            "titulo"=>"Consulta 4",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista left JOIN lleva ON ciclista.dorsal = lleva.dorsal
                WHERE lleva.dorsal IS null;",
        ]);
    }
    public function  actionConsulta4a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
               ->distinct() 
               ->select("ciclista.dorsal, nombre")
               ->leftJoin("lleva","ciclista.dorsal=lleva.dorsal")
               ->where("lleva.dorsal is null"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre'], 
            "enunciado"=>"Dorsal y nombre de los ciclistas queno  hayan llevado algún maillot",
            "titulo"=>"Consulta 4",
            "sql"=>"SELECT DISTINCT ciclista.dorsal, ciclista.nombre FROM ciclista left JOIN lleva ON ciclista.dorsal = lleva.dorsal
                WHERE lleva.dorsal IS null",
        ]);
    }    
    public function  actionConsulta5(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(ciclista.dorsal) FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM maillot INNER JOIN lleva 
                ON maillot.código = lleva.código WHERE color = 'amarillo') c1 USING(dorsal) WHERE c1.dorsal IS NULL")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT ciclista.dorsal, ciclista.nombre FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM maillot INNER JOIN lleva 
            ON maillot.código = lleva.código WHERE color = "amarillo") c1 USING(dorsal) WHERE c1.dorsal IS NULL',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre'], 
            "enunciado"=>"Dorsal y nombre de los ciclistas que  no  han llevado el maillot amarillo",
             "titulo"=>"Consulta 5",
            "sql"=>"SELECT ciclista.nombre, ciclista.dorsal FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM maillot INNER JOIN lleva 
            ON maillot.código = lleva.código WHERE color = 'amarillo') c1 USING(dorsal) WHERE c1.dorsal IS NULL",
        ]);
    }

    public function  actionConsulta5a(){
        // mediante active record
        $ganaa = Lleva::find()
               ->distinct() 
               ->select("dorsal")
               ->innerJoin('maillot', 'maillot.código = lleva.código')
               ->where("color='amarillo'");
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
               ->distinct() 
               ->select("ciclista.dorsal dorsal, nombre")
               ->leftjoin(['c1'=>$ganaa], 'c1.dorsal=ciclista.dorsal')
               ->where("c1.dorsal IS NULL"),
          
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal', 'nombre'], 
             "enunciado"=>"Dorsal y nombre de los ciclistas que no hayan llevado algún maillot",
             "titulo"=>"Consulta 5",
            "sql"=>"SELECT ciclista.nombre, ciclista.dorsal FROM ciclista LEFT JOIN (SELECT DISTINCT lleva.dorsal FROM maillot INNER JOIN lleva 
            ON maillot.código = lleva.código WHERE color = 'amarillo') c1 USING(dorsal) WHERE c1.dorsal IS NULL",
        ]);
    }
    public function  actionConsulta6(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS null")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT etapa.numetapa numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS null',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'], 
            "enunciado"=>"Indicar el numero de las etapas que no tengan puertos",
             "titulo"=>"Consulta 6",
            "sql"=>"SELECT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS null;",
        ]);
    }
    public function  actionConsulta6a(){
        // mediante active record
         $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
               ->select("etapa.numetapa numetapa")
               ->leftJoin("puerto","etapa.numetapa=puerto.numetapa")
               ->where("puerto.numetapa is null"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['numetapa'], 
              "enunciado"=>"Indicar el numero de las etapas que no tengan puertos",
             "titulo"=>"Consulta 6",
            "sql"=>"SELECT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa WHERE puerto.numetapa IS null;",
        ]);
    }
    public function  actionConsulta7(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT AVG(kms) AS kms FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['kms'], 
            "enunciado"=>"Indicar la distancia media de las etapas que no tengan puertos",
            "titulo"=>"Consulta 7",
            "sql"=>"SELECT AVG(etapa.kms) AS promedio FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    public function  actionConsulta7a(){
        // mediante active record
       $dataProvider = new ActiveDataProvider([
            'query' => Etapa::find()
               ->select("avg(kms) kms")
               ->leftJoin("puerto","etapa.numetapa=puerto.numetapa")
               ->where("puerto.numetapa is null"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['kms'], 
             "enunciado"=>"Indicar la distancia media de las etapas que no tengan puertos",
             "titulo"=>"Consulta 7",
            "sql"=>"SELECT AVG(etapa.kms) AS promedio FROM etapa LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL",
        ]);
    }
    
    public function  actionConsulta8(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT COUNT(ciclista.dorsal) FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT COUNT(ciclista.dorsal) AS total FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['total'], 
            "enunciado"=>"Listar el numero de ciclistas que hayan ganado alguna etapa",
            "titulo"=>"Consulta 8",
            "sql"=>"SELECT COUNT(ciclista.dorsal) AS total FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    public function  actionConsulta8a(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
               ->distinct() 
               ->select("count(ciclista.dorsal) total")
                ->leftJoin("etapa","etapa.dorsal=ciclista.dorsal")
               ->where("etapa.dorsal is null"),
            ]);
        return $this->render("resultado", [
             "resultados"=>$dataProvider,
             "campos"=>['total'], 
            "enunciado"=>"Listar el numero de ciclistas que no hayan ganado alguna etapa",
            "titulo"=>"Consulta 8",
            "sql"=>"SELECT COUNT(ciclista.dorsal) AS total FROM ciclista LEFT JOIN etapa ON ciclista.dorsal = etapa.dorsal WHERE etapa.dorsal IS NULL",
        ]);
    }
    public function  actionConsulta9(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(DISTINCT ciclista.dorsal) FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal) LEFT JOIN 
                  puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT DISTINCT ciclista.dorsal FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal) LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa
            WHERE puerto.numetapa IS NULL',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar el dorsal de ciclistas que hayan ganado alguna etapa sin puerto",
            "titulo"=>"Consulta 9",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal) LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa
            WHERE puerto.numetapa IS NULL",
        ]);
    }
    public function  actionConsulta9a(){
        // mediante active record
          $dataProvider = new ActiveDataProvider([
            'query' => Ciclista::find()
               ->distinct() 
               ->select("ciclista.dorsal dorsal")
               ->innerJoinwith('etapas', true)
               ->leftJoin('puerto', 'etapa.numetapa = puerto.numetapa')
               ->where("puerto.numetapa IS NULL"),
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado alguna etapa sin puerto",
            "titulo"=>"Consulta 9",
            "sql"=>"SELECT DISTINCT ciclista.dorsal FROM (ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal) LEFT JOIN puerto ON etapa.numetapa = puerto.numetapa
            WHERE puerto.numetapa IS NULL",
        ]);
    }
    public function  actionConsulta10(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(c1.dorsal) FROM (SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa
                ON ciclista.dorsal = etapa.dorsal LEFT JOIN
                puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL) c1 
                LEFT JOIN (SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa
                ON ciclista.dorsal = etapa.dorsal INNER JOIN 
                puerto ON etapa.numetapa = puerto.numetapa) c2 
                on c1.dorsal = c2.dorsal  WHERE c2.dorsal IS null")
              ->queryScalar();
        
      $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT c1.dorsal FROM (SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa
                ON ciclista.dorsal = etapa.dorsal LEFT JOIN
                puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL) c1 
                LEFT JOIN (SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa
                ON ciclista.dorsal = etapa.dorsal INNER JOIN 
                puerto ON etapa.numetapa = puerto.numetapa) c2 
                on c1.dorsal = c2.dorsal  WHERE c2.dorsal IS null',
            ]);
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'], 
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado unicamnete etapas que no tengan puerto",
            "titulo"=>"Consulta 10",
            "sql"=>"SELECT c1.dorsal FROM (SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa
                ON ciclista.dorsal = etapa.dorsal LEFT JOIN
                puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL) c1 
                LEFT JOIN (SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa
                ON ciclista.dorsal = etapa.dorsal INNER JOIN 
                puerto ON etapa.numetapa = puerto.numetapa) c2 
                on c1.dorsal = c2.dorsal  WHERE c2.dorsal IS null",
        ]);
    }
    public function  actionConsulta10a(){
        // mediante active record
        /*SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal LEFT JOIN
        puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL;*/
        $c1 = Ciclista::find()
               ->distinct() 
               ->select("ciclista.dorsal dorsal")
               ->innerJoin('etapa', 'ciclista.dorsal = etapa.dorsal')
               ->leftJoin('puerto', 'etapa.numetapa = puerto.numetapa')
               ->where("puerto.numetapa IS NULL");
        /*SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa ON ciclista.dorsal = etapa.dorsal INNER JOIN 
        puerto ON etapa.numetapa = puerto.numetapa;*/
        $c2 = Ciclista::find()
               ->distinct() 
               ->select("ciclista.dorsal")
               ->innerJoin('etapa', 'ciclista.dorsal = etapa.dorsal')
               ->innerJoin('puerto', 'etapa.numetapa = puerto.numetapa');
        $query = new Query();
        $dataProvider = new ActiveDataProvider([
            'query' => $query->from(['c1'=>$c1])
              ->select("c1.dorsal dorsal")
              ->leftJoin(['c2'=>$c2], 'c1.dorsal = c2.dorsal')
              ->where("c2.dorsal IS NULL"),
          ]);
        
        return $this->render("resultado", [
            "resultados"=>$dataProvider,
           "campos"=>['dorsal'], 
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado unicamnete etapas que no tengan puerto",
            "titulo"=>"Consulta 10",
            "sql"=>"SELECT c1.dorsal FROM (SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa
                ON ciclista.dorsal = etapa.dorsal LEFT JOIN
                puerto ON etapa.numetapa = puerto.numetapa WHERE puerto.numetapa IS NULL) c1 
                LEFT JOIN (SELECT DISTINCT ciclista.dorsal dorsal FROM ciclista INNER JOIN etapa
                ON ciclista.dorsal = etapa.dorsal INNER JOIN 
                puerto ON etapa.numetapa = puerto.numetapa) c2 
                on c1.dorsal = c2.dorsal  WHERE c2.dorsal IS null",
        ]);
    }
    
    }