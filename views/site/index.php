<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de selección!</h1>
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">
         <div class="row">
             <!--
             Boton de consulta
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 1</h3>
                    <p>Nombre y edad de los ciclistas que no han ganado etapas</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta1a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta1'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <!--
             Boton de consulta dos
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 2</h3>
                    <p>Nombre y edad de los ciclistas que no han ganado puertos</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta2a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta2'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             
             <!--
             Boton de consulta tres
             -->
             
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta otra</h3>
                    <p>Nombre y edad de los ciclistas que no han ganado etapas y puertos</p>
                    <p>
                    <?= Html::a('Active record', ['site/consultatresa'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consultatres'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div>   
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 3</h3>
                    <p>Listar el director de los equipos que tengan ciclistas que no hayan ganado alguna etapa</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta3a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta3'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
               <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 4</h3>
                    <p>Dorsal y nombre de los ciclistas que no hayan llevado algún maillot</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta4a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta4'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
             <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 5</h3>
                    <p>Dorsal y nombre de los ciclistas que no  han llevado el maillot amarillo</p>
                    <?= Html::a('Active record', ['site/consulta5a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta5'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 6</h3>
                    <p>Indicar el numero de las etapas que no tengan puertos</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta6a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta6'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 7</h3>
                    <p>Indicar la distancia media de las etapas que no tengan puertos</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta7a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta7'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            
           <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 8</h3>
                    <p>listar el numero de ciclistas que No hayan ganado alguna etapa</p>
                    <?= Html::a('Active record', ['site/consulta8a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta8'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
         </div> 
        <div class="row">
             <!--
             Boton de consulta
             -->
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 9</h3>
                    <p>Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta9a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta9'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
            <div class="col-sm-6 col-md-4">
            <div class="thumbnail">
                <div class="caption">
                    <h3>Consulta 10</h3>
                    <p>Listar el dorsal de los ciclistas que hayan ganado unicamnete etapas que no tengan puerto</p>
                    <p>
                    <?= Html::a('Active record', ['site/consulta10a'], ['class' => 'btn btn-primary']);?>
                    <?= Html::a('DAO', ['site/consulta10'], ['class' => 'btn btn-default']);?>
                    </p>
                </div>
            </div>
            </div>
          </div> 
    </div>
</div>
